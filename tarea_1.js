// 2) ¿Cuántos registros arrojo el comando count?
// Comandos:
> use students;
> db.grades.count();
/*
Respuesta:
Se arrojaron 800 registros.
*/

// 3) Encuentra todas las calificaciones del estudiante
// con el id numero 4.
// Comandos:
> db.grades.find({student_id:4}).pretty();
/*
Respuesta:
{
	"_id" : ObjectId("50906d7fa3c412bb040eb587"),
	"student_id" : 4,
	"type" : "exam",
	"score" : 87.89071881934647
}
{
	"_id" : ObjectId("50906d7fa3c412bb040eb588"),
	"student_id" : 4,
	"type" : "quiz",
	"score" : 27.29006335059361
}
{
	"_id" : ObjectId("50906d7fa3c412bb040eb589"),
	"student_id" : 4,
	"type" : "homework",
	"score" : 5.244452510818443
}
{
	"_id" : ObjectId("50906d7fa3c412bb040eb58a"),
	"student_id" : 4,
	"type" : "homework",
	"score" : 28.656451042441
}
*/

// 4) ¿Cuántos registros hay de tipo exam?
// Comandos:
> db.grades.find({type:"exam"}).count();
/*
Respuesta:
Se arrojaron 200 registros.
*/

// 5) ¿Cuántos registros hay de tipo homework?
// Comandos:
> db.grades.find({type:"homework"}).count();
/*
Respuesta:
Se arrojaron 400 registros.
*/

// 6) ¿Cuántos registros hay de tipo quiz?
// Comandos:
> db.grades.find({type:"quiz"}).count();
/*
Respuesta:
Se arrojaron 200 registros.
*/

// 7) Elimina todas las calificaciones del
// estudiante con el id numero 3.
// Comandos:
> db.grades.deleteMany({student_id:3});
/*
Respuesta:
{ "acknowledged" : true, "deletedCount" : 4 }
*/

// 8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea?
// Comandos:
> db.grades.find({score:75.29561445722392});
/*
Respuesta:
{ "_id" : ObjectId("50906d7fa3c412bb040eb59e"), "student_id" : 9, "type" : "homework", "score" : 75.29561445722392 }
*/

// 9) Actualiza las calificaciones del registro con el
// uuid 50906d7fa3c412bb040eb591 por 100.
// Comandos:
> db.grades.updateMany({"_id" : ObjectId("50906d7fa3c412bb040eb591")}, {$set:{score:100}});
> db.grades.find({"_id" : ObjectId("50906d7fa3c412bb040eb591")}).pretty(); // Para comprobar el cambio.
/*
Respuesta:
db.grades.updateMany({"_id" : ObjectId("50906d7fa3c412bb040eb591")}, {$set:{score:100}});

{
	"_id" : ObjectId("50906d7fa3c412bb040eb591"),
	"student_id" : 6,
	"type" : "homework",
	"score" : 100
}

*/

// 10) A qué estudiante pertenece esta calificación.
// Comandos:
> db.grades.find({score:100}).pretty();
/*
Respuesta:
{
	"_id" : ObjectId("50906d7fa3c412bb040eb591"),
	"student_id" : 6,
	"type" : "homework",
	"score" : 100
}
*/
